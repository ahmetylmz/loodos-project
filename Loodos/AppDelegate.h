//
//  AppDelegate.h
//  Loodos
//
//  Created by xeon on 7.01.2019.
//  Copyright © 2019 xeon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

