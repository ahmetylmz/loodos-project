//
//  ViewController.m
//  Loodos
//
//  Created by xeon on 7.01.2019.
//  Copyright © 2019 xeon. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.remoteConfig = [FIRRemoteConfig remoteConfig];
    [self fetchConfig];
    
}

- (void)fetchConfig {
    
    long expirationDuration = 360;

    // README for more information.
    [self.remoteConfig fetchWithExpirationDuration:expirationDuration completionHandler:^(FIRRemoteConfigFetchStatus status, NSError *error) {
        if (status == FIRRemoteConfigFetchStatusSuccess) {
            NSLog(@"Config fetched!");
            [self.remoteConfig activateFetched];
        } else {
            NSLog(@"Config not fetched");
            NSLog(@"Error %@", error.localizedDescription);
        }
        [self displayText];
    }];
    // [END fetch_config_with_callback]
}

- (void)displayText {
    // [START get_config_value]
    NSString *message = self.remoteConfig[@"loodosText"].stringValue;
    // [END get_config_value]
    if (self.remoteConfig[@"loodosText"].stringValue) {
        message = [message uppercaseString];
    }
    self.splashLabel.text = message;
    [NSTimer scheduledTimerWithTimeInterval:3.0
                                     target:self
                                   selector:@selector(showNextController:)
                                   userInfo:nil
                                    repeats:NO];
}

- (void) showNextController:(NSTimer*)t {
    
    [self performSegueWithIdentifier:@"3second" sender:self];
    
}
- (IBAction)nextPage:(id)sender {
     [self performSegueWithIdentifier:@"3second" sender:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
