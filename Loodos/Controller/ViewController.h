//
//  ViewController.h
//  Loodos
//
//  Created by xeon on 7.01.2019.
//  Copyright © 2019 xeon. All rights reserved.
//

#import <UIKit/UIKit.h>
@import FirebaseRemoteConfig;

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *splashLabel;
@property (nonatomic, strong) FIRRemoteConfig *remoteConfig;

@end

