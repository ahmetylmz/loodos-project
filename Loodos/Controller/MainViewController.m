//
//  MainViewController.m
//  Loodos
//
//  Created by xeon on 7.01.2019.
//  Copyright © 2019 xeon. All rights reserved.
//

#import "MainViewController.h"
#import "AFNetworking.h"
#import <UIKit/UIKit.h>
@import Firebase;

@interface MainViewController ()

@end

@implementation MainViewController 

UIActivityIndicatorView *activityIndicatorView;
NSString *url;

- (void)viewDidLoad {
    [super viewDidLoad];
    url = @"http://www.omdbapi.com/?t=Game%20Of%20Thrones&Season=2&apikey=9bb8d431";
    [self omdbRequest];
    [self indicatorSetup];
}

-(void)indicatorSetup {

    activityIndicatorView= [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activityIndicatorView.hidesWhenStopped = YES;
    activityIndicatorView.center = self.view.center;
    [self.view addSubview:activityIndicatorView];
    [activityIndicatorView startAnimating];
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
    url = [NSString stringWithFormat:@"%@/%@/%s", @"http://www.omdbapi.com/?t=", self.searchBar.text, "&apikey=9bb8d431"];
    url = [url stringByReplacingOccurrencesOfString:@" " withString:@"%20"];

    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(omdbRequest) object:nil];
    [self performSelector:@selector(omdbRequest) withObject:nil afterDelay:0.5];
    
}

- (void) omdbRequest {
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];

    [manager GET:url parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
        
        [activityIndicatorView stopAnimating];
        
        NSMutableArray <NSDictionary *> *response = data[@"Episodes"];
        if(response != nil){
            NSMutableArray <Episode *> *episodeObject = [[NSMutableArray <Episode *> alloc] init];
            
            for (int i=0; i<response.count; i++) {
                Episode *ep = [[Episode alloc] init];
                ep.title = response[i][@"Title"];
                ep.episode = response[i][@"Episode"];
                ep.released = response[i][@"Released"];
                ep.imdbRating = response[i][@"imdbRating"];
                ep.imdbID = response[i][@"imdbID"];
                [episodeObject addObject:ep];
                
            }
            
            Serie *serie = [[Serie alloc] init];
            
            serie.title = data[@"Title"];
            serie.season = data[@"Season"];
            serie.totalSeasons = data[@"totalSeasons"];
            serie.response = data[@"Response"];
            serie.episodes = episodeObject;
            self.seriesArray = [[NSMutableArray <Serie *> alloc] init];
            
            [self.seriesArray addObject:serie];
            [self.filmsArray removeAllObjects];
            [self.tableview reloadData];
        }else if(data[@"Ratings"] != nil){
            NSMutableArray <NSDictionary *> *response = data[@"Ratings"];
            NSMutableArray <Rating * > *ratingObject = [[NSMutableArray <Rating *> alloc] init];
            
            for (int i=0; i<response.count; i++) {
                Rating *rt = [[Rating alloc] init];
                rt.source = response[i][@"Source"];
                rt.value = response[i][@"Value"];
                [ratingObject addObject:rt];
                
            }
            
            Film *film = [[Film alloc] init];
            
            film.title = data[@"Title"];
            film.year = data[@"Year"];
            film.released = data[@"Released"];
            film.genre = data[@"Genre"];
            film.poster = data[@"Poster"];
            film.actors = data[@"Actors"];
            film.ratings = ratingObject;
            self.filmsArray = [[NSMutableArray <Film *> alloc] init];
            
            [self.filmsArray addObject:film];
            [self.seriesArray removeAllObjects];
            [self.tableview reloadData];
        }else {
            [self failAlert];
        }
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"Error: %@", error);
    }];
    
}

- (void) failAlert {
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"İçerik Bulunamadı" message:@"" preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Tamam",@"confirm") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }]];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [[UITableViewCell alloc] init];
    if(self.seriesArray.count >0){
        cell.textLabel.text = self.seriesArray[0].episodes[indexPath.row].title;
        cell.detailTextLabel.text = self.seriesArray[0].episodes[indexPath.row].released;
    }else{
        cell.textLabel.text = self.filmsArray[0].title;
        cell.detailTextLabel.text = self.filmsArray[0].genre;
    }
   
    
    return cell;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(self.seriesArray.count >0){
         return self.seriesArray[0].episodes.count;
    }else{
        return self.filmsArray.count;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self detailViewSetup:(int)indexPath.row];
    

    self.detailView.alpha = 0.0;
    [self.detailView setHidden:false];
    [UIView animateWithDuration:0.4
                     animations:^{
                         self.detailView.alpha = 1.0;
                     }
                     completion:^(BOOL finished) {
                        
                     }];
}

- (IBAction)closeDetail:(UIButton *)sender {

    [UIView animateWithDuration:0.3
                     animations:^{
                         self.detailView.alpha = 0.0;
                     }
                     completion:^(BOOL finished) {
                         [self.detailView setHidden:true];
                     }];
}

- (void) detailViewSetup:(int)index {
    
    if(self.seriesArray.count > 0) {
        self.detaiViewTitle.text = self.seriesArray[0].title;
        self.detailViewLbl1.text = self.seriesArray[0].episodes[index].title;
        self.detailViewLbl2.text = self.seriesArray[0].episodes[index].released;
        self.detailViewLbl3.text = self.seriesArray[0].episodes[index].episode;
        self.detailViewLbl4.text = self.seriesArray[0].episodes[index].imdbRating;
        self.detailViewLbl5.text = self.seriesArray[0].episodes[index].imdbID;
        [FIRAnalytics logEventWithName:kFIREventSelectContent
                            parameters:@{
                                         kFIRParameterItemID:[NSString stringWithFormat:@"ID: %@", self.seriesArray[0].title],
                                         kFIRParameterItemName:self.seriesArray[0].episodes[index].title
                                         }];
    }else {
        self.detaiViewTitle.text = self.filmsArray[0].title;
        self.detailViewLbl1.text = [NSString stringWithFormat:@"%@ / %@" ,self.filmsArray[0].ratings[index].source , self.filmsArray[0].ratings[index].value] ;
        self.detailViewLbl2.text = self.filmsArray[0].year;
        self.detailViewLbl3.text = self.filmsArray[0].released;
        self.detailViewLbl4.text = self.filmsArray[0].genre;
        self.detailViewLbl5.text = self.filmsArray[0].actors;
        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:self.filmsArray[0].poster]];
        self.detailCover.image = [[UIImage alloc] initWithData:data];
        
        [FIRAnalytics logEventWithName:kFIREventSelectContent
                            parameters:@{
                                         kFIRParameterItemID:[NSString stringWithFormat:@"ID: %@", self.filmsArray[0].year],
                                         kFIRParameterItemName:self.filmsArray[0].title
                                         }];
    }
}

@end
