//
//  MainViewController.h
//  Loodos
//
//  Created by xeon on 7.01.2019.
//  Copyright © 2019 xeon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Serie.h"
#import "Film.h"
@interface MainViewController : UIViewController <UISearchBarDelegate,UITableViewDelegate,UITableViewDataSource>  
@property (weak, nonatomic) IBOutlet UITableView *tableview;
@property (weak, nonatomic) IBOutlet UIView *detailView;
@property (weak, nonatomic) IBOutlet UIView *searchView;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong , nonatomic) NSMutableArray <Serie *> *seriesArray;
@property (strong , nonatomic) NSMutableArray <Film *> *filmsArray;

@property (weak, nonatomic) IBOutlet UILabel *detaiViewTitle;
@property (weak, nonatomic) IBOutlet UILabel *detailViewLbl1;
@property (weak, nonatomic) IBOutlet UILabel *detailViewLbl2;
@property (weak, nonatomic) IBOutlet UILabel *detailViewLbl3;
@property (weak, nonatomic) IBOutlet UILabel *detailViewLbl4;
@property (weak, nonatomic) IBOutlet UILabel *detailViewLbl5;
@property (weak, nonatomic) IBOutlet UIImageView *detailCover;


@end
