//
//  Film.h
//  Loodos
//
//  Created by xeon on 8.01.2019.
//  Copyright © 2019 xeon. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Film;
@class Rating;

NS_ASSUME_NONNULL_BEGIN

#pragma mark - Object interfaces

@interface Film : NSObject
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *year;
@property (nonatomic, copy) NSString *rated;
@property (nonatomic, copy) NSString *released;
@property (nonatomic, copy) NSString *runtime;
@property (nonatomic, copy) NSString *genre;
@property (nonatomic, copy) NSString *director;
@property (nonatomic, copy) NSString *writer;
@property (nonatomic, copy) NSString *actors;
@property (nonatomic, copy) NSString *plot;
@property (nonatomic, copy) NSString *language;
@property (nonatomic, copy) NSString *country;
@property (nonatomic, copy) NSString *awards;
@property (nonatomic, copy) NSString *poster;
@property (nonatomic, copy) NSArray<Rating *> *ratings;
@property (nonatomic, copy) NSString *metascore;
@property (nonatomic, copy) NSString *imdbRating;
@property (nonatomic, copy) NSString *imdbVotes;
@property (nonatomic, copy) NSString *imdbID;
@property (nonatomic, copy) NSString *type;
@property (nonatomic, copy) NSString *dvd;
@property (nonatomic, copy) NSString *boxOffice;
@property (nonatomic, copy) NSString *production;
@property (nonatomic, copy) NSString *website;
@property (nonatomic, copy) NSString *response;
@end

@interface Rating : NSObject
@property (nonatomic, copy) NSString *source;
@property (nonatomic, copy) NSString *value;
@end

NS_ASSUME_NONNULL_END

