//
//  Serie.h
//  Loodos
//
//  Created by xeon on 8.01.2019.
//  Copyright © 2019 xeon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class Serie;
@class Episode;

NS_ASSUME_NONNULL_BEGIN

#pragma mark - Object interfaces

@interface Serie : NSObject
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *season;
@property (nonatomic, strong) NSString *totalSeasons;
@property (nonatomic, strong) NSArray<Episode *> *episodes;
@property (nonatomic, strong) NSString *response;

-(id) init;
@end

@interface Episode : NSObject
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *released;
@property (nonatomic, strong) NSString *episode;
@property (nonatomic, strong) NSString *imdbRating;
@property (nonatomic, strong) NSString *imdbID;
-(id) init;
@end

NS_ASSUME_NONNULL_END
